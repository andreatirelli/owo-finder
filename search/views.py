import re
from django.shortcuts import render
from django.core.paginator import EmptyPage, Paginator
from search.bin.searcher import searchDoc

# Create your views here.
def index(request):
    return render(request, 'index.html')

def search(request):
    if 'search' in request.GET:
        search = request.GET['search']
        results, counter, timer = searchDoc(search)
        request.session['results'] = results
        request.session['results_num'] = counter
        request.session['timer'] = timer

    # Paginator
    paginator = Paginator(request.session['results'], 100)
    page_num = request.GET.get('page')
    results_obj = paginator.get_page(page_num)

    return render(request, 'search.html', context = {
        'results': results_obj,
        'results_num': request.session['results_num'],
        'timer': request.session['timer']
    })