from urllib.request import Request, urlopen
import urllib.parse
import json
import os
import time

from bs4 import BeautifulSoup


# Create the json fine with some data
def creare_json(path, data):
	try:
		os.mkdir(path)
	except OSError:
		print ("Creation of the directory %s failed" % path)

	with open(path + '/data.json', 'w', encoding='utf8') as outfile:
		json.dump(data, outfile)


# Get the top X anime from MAL
def getTopAnimeList(top_of):
	data = {}
	data['top'] = []

	for x in range(0, top_of, 50):
		url = 'https://myanimelist.net/topanime.php?type=bypopularity&limit=' + str(x)
		f = urllib.request.urlopen(url)
		soup = BeautifulSoup(f, 'html.parser')

		# Container con le info di ogni riga della tabella del sito
		container = soup.findAll("tr", {"class": "ranking-list"})

		for c in container:
			rank = c.find("td", {"class": "rank ac"})
			title = c.find("h3", {"class": "hoverinfo_trigger fl-l fs14 fw-b anime_ranking_h3"})

			l_url = title.a['href']
			rank = str(rank.span.text)
			title = title.a.text

			clean = title.replace(' ', '_')
			clean = clean.replace(',', '_')
			clean = clean.replace(';', '_')
			clean = clean.replace(':', '_')
			clean = clean.replace('.', '')

			l_url = l_url.replace('/'+ clean, '')

			data['top'].append({
				'title': title,
				'rank': rank,
				'url': l_url
			})
			print("Found -> " + title)
		# Stop for 1s, to prevent IP block form MAL for to much request in a short time
		time.sleep(0.5)

	creare_json("json", data)


def json4Anime(url):
	req = Request(url, headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0'})
	# f = urllib.request.urlopen(url)
	f = urlopen(req)
	soup = BeautifulSoup(f, 'html.parser')

	container = soup.find("div", {"id": "contentWrapper"});
	divInfo = container.findAll("div", {"class": "spaceit_pad"})

	synonyms, entitle = "", ""
	for d in divInfo:
		# entitle
		span = d.find("span", {"class": "dark_text"})
		if span != None and span.text == "English:":
			entitle = d.text.replace('English: ', '');
			entitle = entitle.replace('\n', '')
			entitle = entitle.replace('  ', '')

		if span != None and span.text == "Synonyms:":
			synonyms = d.text.replace('Synonyms: ', '');
			synonyms = synonyms.replace('\n', '');
			synonyms = synonyms.replace('  ', '')

	divInfo = container.findAll("div", {"class": "spaceit"})
	ep = None
	tag = []

	# i = 0
	for d in divInfo:
		# Episodes
		all = d.find("span", {"class": "dark_text"})
		if all != None and all.text == "Episodes:":
			ep = d.text.replace('\nEpisodes:\n  ', '')
			ep = ep.replace('\n  ', '')


	divInfo = container.findAll("div")
	for d in divInfo:
		for span in d.findAll('span', itemprop="genre"):
			tag.append(span.string)

	synopsis = container.find('p', itemprop="description").text
	synopsis = synopsis.replace('\n\r\n[Written by MAL Rewrite]', '')
		
	return entitle, synonyms, ep, tag, synopsis


def loadTopList(path, buffer):
	count = 0;
	f = open(path + '/data.json', 'r')
	jdata = json.load(f)
	f.close()

	for i in jdata['top']:
		print("processing N -> " + i['rank'] + " - " + i['title'])

		series = {}
		series['data'] = []

		entitle, synonyms, ep, tag, synopsis = json4Anime(i['url'])

		series['data'].append({
			'title' :i['title'],
			'en-title': entitle,
			'synonyms': synonyms,
			'episodes': ep,
			'tags': tag,
			'synopsis': synopsis
		})
		with open(path + '/' + i['rank'] + '.json', 'w', encoding='utf8') as outfile:
			json.dump(series, outfile)
		if count > buffer:
			time.sleep(300)
			count = 0
		else: count += 1