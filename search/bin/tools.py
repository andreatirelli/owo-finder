import requests
import os
import errno

# URL for the HTTP API request for the data.
url = 'https://graphql.anilist.co'

# Define the headers for the HTTP request packet
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0'
}

# Define the query to send to the API
query = '''
query ($id: Int) { # Define which variables will be used in the query (id)
  Media (id: $id) {
        id
        idMal
        type
        title {
          romaji
          english
          native
        }
        format
        status
        description
        startDate {
          year
          month
          day
        }
        endDate {
          year
          month
          day
        }
        season
        seasonYear
        seasonInt
        episodes
        duration
        chapters
        volumes
        countryOfOrigin
        source
        coverImage {
          extraLarge
          medium
          color
        }
        bannerImage
        genres
        synonyms
        averageScore
        tags {
          id
          name
          isAdult
        }
        characters {
          nodes {
            id
            name {
              first
              middle
              last
              full
            }
            image {
              large
              medium
            }
            gender
            dateOfBirth {
              year
              month
              day
            }
            age
            description
            siteUrl
          }
        }
        studios {
          nodes {
            id
            name
            siteUrl
          }
        }
        isAdult
        externalLinks {
          id
          url
          site
        }
        siteUrl
    }
}
'''

def create_json(path, json_data, name, ):
    try:
      os.mkdir(path)
    except OSError as ex:
      if ex.errno is not errno.EEXIST:
        print ("Creation of the directory %s failed" % path)
      pass

    with open(path + '/' + str(name) + '.json', 'w', encoding='utf8') as json_file:
        json_file.write(json_data)
        json_file.close()

def send_query(index):
    variables = {
        'id': index
    }
    print(">> " + str(variables['id']))
    response = requests.post(url, json={'query': query, 'variables': variables}, headers=headers)
    return response