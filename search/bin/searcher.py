from whoosh import index
from whoosh.fields import *
from whoosh.qparser import *
from whoosh import qparser
from whoosh import sorting
from whoosh import scoring
from nltk.corpus import wordnet
import time

ix = index.open_dir("indexdir", indexname="anime")

searcher = ix.searcher(weighting=scoring.TF_IDF)

parserKeyword = MultifieldParser(["title", "en_title", "synonyms", "description"], schema=ix.schema, group=qparser.OrGroup)
parserThesaurus = QueryParser("content", schema=ix.schema, group=qparser.OrGroup)

parserKeyword.add_plugin(GtLtPlugin)

thesPattern = '#'
operators = ("AND", "OR", "NOT")

def cercaSyn(term):
    words = []
    try:
        syns = wordnet.synsets(term)
        for syn in syns:
            words.append(syn.name())
        words = ' '.join([elem for elem in words])
        return words
    except:
        return None



def searchDoc(query):
    start_time = time.time()

    query = query.split()
    
    finalQuery = ''
    subQuery = []
    phrase = False

    for word in query:

        post = []

        if word.startswith('"') or word.endswith('"'):
            if phrase == False:
                phrase = True
            else:
                phrase = False
            subQuery.append(word)
            continue

        if phrase==False:
            while word.startswith('('):
                subQuery.append('(')
                word = word[1:]
                
            while word.endswith(')'):
                post.append(')')
                word = word[:-1]
                
            if word.endswith(thesPattern) and phrase == False:
                app = word.replace(thesPattern, '')
                app = cercaSyn(app)
                
                if app:
                    subQuery.append(str(parserThesaurus.parse(app)))
            else:
                subQuery.append(word)
            
            while post:
                subQuery.append(post.pop())
        else:
            subQuery.append(word)   

    try:
        finalQuery = parserKeyword.parse(' '.join(subQuery))
        results = []
        print(finalQuery, '\n')
        docScore = sorting.ScoreFacet()
        animeScore = sorting.FieldFacet("score", reverse=True)
        
        results = searcher.search(finalQuery, limit=None, sortedby=[docScore, animeScore])
        # print("Risultati: ",len(results))
        
        res = []
        for x in results:
            res.append((x['title'], x['url'], x['image'], x['score']))
            
        # jsonRes=json.dumps(res)
    except Exception as e:
        print(e)
        res = []
    # searcher.close()
    return res, len(res), round((time.time() - start_time), 2)