import json
from time import sleep

from bin.tools import send_query, create_json


def fetch_data():
    index = 1 # 107368 # 73619 # 67643 # 59184 # 51714 # 19584 # 19528 # 15658 # 14759 # 9180 # 540 # 1
    timer = 0
    while(index < 137828):
        r = send_query(index)
        print(">> " + str(r))
        if r.ok:
            my_json = r.json()
            create_json('dump', json.dumps(my_json), index)
        if int(r.headers['X-RateLimit-Remaining']) == 0:
            print(">> !!API request expired = " + str(r.headers['X-RateLimit-Remaining'])
            + "waiting 2 minutes!")
            sleep(1 * 60) # sleep for 2 minutes
            timer += 1
        else:
            print(">> Still avaible API request= " + str(r.headers['X-RateLimit-Remaining']))
        # if timer == 9:
        #     timer = 0
        #     sleep(30 * 60)
        index += 1