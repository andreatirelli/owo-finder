import os
import json
from whoosh import index
from whoosh.qparser import *
from whoosh.fields import *
from whoosh.analysis import *
from nltk.corpus import wordnet as wn
from nltk.wsd import lesk
from rake_nltk import Rake


dirog = os.getcwd()
docDir = os.getcwd() + '\dump\dump'

#dizionario dei tags
tagsDict = {
    'Magic':'magic.n.01', 'Triads':'mafia.n.01', 'Philosophy':'philosophy.n.02', 'Skeleton':'skeletal_system.n.01', 'Swimming':'swimming.n.01',
    'Writing':'writing.n.01 write.v.01', 'Vikings':'viking.n.01', 'Amnesia':'amnesia.n.01', 'Henshin':'', 'Card Battle':'card_game.n.01 card.n.01 battle.n.01',
    'Drugs':'drug.n.01', 'Slavery':'slavery.n.02 slave.n.01', 'Photography':'photography.n.01 photograph.n.01', 'Advertisement':'', 'Reformation':'',
    'Cars':'car.n.01', 'Circus':'circus.n.01', 'Gore':'gore.v.01', 'Assassins':'assassin.n.01', 'Meta':'',
    'Drawing':'drawing.n.01 draw.v.06', 'Football':'football.n.01 soccer.n.01', 'Archery':'archery.n.01 bow.n.04', 'College':'college.n.02 school.n.01', 'Vampire':'vampire.n.01',
    'Ships':'ship.n.01 boat.n.01', 'Basketball':'basketball.n.01 basket.n.04 sport.n.01', 'Poker':'poker.n.02 ', 'Family Life':'family.n.02', 'Succubus':'monster.n.04',
    'Kaiju':'giant.n.04', 'Cyborg':'cyborg.n.01', 'Love Triangle':'love.n.01 love_story.n.01', 'Fitness':'sport.n.01', 'Language Barrier':'language.n.01',
    'Dancing':'dance.v.02', 'Karuta':'table_game.n.01', 'Mopeds':'motorcycle.n.01', 'Angels':'angel.n.01', 'Ghost':'ghost.n.03',
    'Band':'band.n.02', 'Kemonomimi':'', 'Elf':'elf.n.01', 'Foreign':'', 'Fairy Tale':'fairytale.n.02', 'Food':'food.n.01',
    'Alternate Universe':'', 'Superhero':'superpower.n.01 hero.n.01', 'Swordplay':'play.n.17 sword.n.01', 'Monster Girl':'monster.n.01', 'Shapeshifting':'',
    'Samurai':'samurai.n.01', 'Bar':'bistro.n.01', 'Gangs':'gang.n.01', 'Ice Skating':'ice_skating.n.01', 'Coming of Age':'grow_up.v.01',
    'Sumo':'sumo.n.01', 'Revenge':'revenge.v.01', 'Iyashikei':'', 'Urban Fantasy':'', 'E-Sports':'computer_game.n.01', 'Anachronism':'anachronism.n.01',
    'Baseball':'baseball.n.01 sport.n.01', 'Shogi':'shogi.n.01', 'Educational':'educational.a.01', 'Time Skip':'', 'Tragedy':'tragedy.n.02', 'Urban':'urban.a.01',
    'Animals':'animal.n.01', 'Table Tennis':'table_tennis.n.01 sport.n.01', 'Bisexual':'bisexual.n.01', 'Aliens':'extraterrestrial_being.n.01', 'Outdoor':'outdoor.a.01',
    'Musical':'musical.n.01 music.n.01', 'Heterosexual':'heterosexual.n.01', 'Environmental':'environmental.a.01', 'Body Swapping':'', 'Post-Apocalyptic':'apocalyptic.s.01',
    'Time Manipulation':'time.n.01', 'Maids':'maid.n.01', 'Idol':'idol.n.02', 'Cultivation':'training.n.01', 'Lacrosse':'lacrosse.n.01 sport.n.01', 'Trains':'train.n.01',
    'Firefighters':'fireman.n.04', 'Goblin':'goblin.n.01 monster.n.01', 'Cult':'cult.n.01', 'Gods':'god.n.01', 'Rehabilitation':'rehabilitation.n.01', 'Cosplay':'',
    'Yakuza':'yakuza.n.02', 'Aviation':'aviation.n.03', 'Monster Boy':'monster.n.01', 'Dullahan':'monster.n.01', 'Boxing':'boxing.n.01',
    'Cyberpunk':'cyberpunk.n.02', 'Artificial Intelligence':'artificial_intelligence.n.01', 'Suicide':'suicide.n.01', 'Adoption':'adoption.n.02',
    'Reincarnation':'reincarnation.n.01', 'Augmented Reality':'', 'Rakugo':'', 'Oiran':'', 'Chimera':'chimera.n.01', 'Dystopian':'dystopian.a.01',
    'Afterlife':'afterlife.n.01', 'Calligraphy':'calligraphy.n.01', 'Super Robot':'automaton.n.02', 'Virtual World':'virtual.s.02', 'Robots':'automaton.n.02',
    'Pandemic':'pandemic.n.01 ', 'Age Regression':'regression.n.04 ', 'Tennis':'tennis.n.01 sport.n.01', 'Mythology':'mythology.n.01', 'Anti-Hero':'',
    'Delinquents':'delinquent.n.01', 'Asexual':'asexual.a.01', 'Detective':'detective.n.02', 'Parkour':'', 'Athletics':'sport.n.01', 'MartialArts':'martial_art.n.01 sport.n.01',
    'Badminton':'badminton.n.01 sport.n.01', 'School Club':'club.n.02', 'Volleyball':'volleyball.n.01 sport.n.01', 'Medicine':'medicine.n.01', 'Tanks':'tank.n.01 ', 'Survival':'survival.n.03',
    'Space Opera':'outer_space.n.01', 'Mermaid':'mermaid.n.01', 'Dinosaurs':'dinosaur.n.01', 'Parody':'parody.n.01', 'Autobiographical':'autobiographical.a.01',
    'Dragons':'dragon.n.01', 'Espionage':'espionage.n.01', 'Twins':'twin.n.01', 'Nun': 'nun.n.01', 'Surfing':'surfing.n.01', 'Cannibalism':'cannibalism.n.01',
    'Satire':'sarcasm.n.01', 'School':'school.n.01', 'Conspiracy':'conspiracy.n.02 ', 'Software Development':'software.n.01', 'Witch':'witch.n.02', 'Military':'military.n.01',
    'Motorcycles':'motorcycle.n.01', 'Gambling':'gambling.n.01', 'Video Games':'computer_game.n.01', 'Rural':'rural.a.01', 'Economics':'economics.n.01', 'Go':'go.n.04',
    'Torture':'torture.v.02', 'Ninja':'ninja.n.01', 'Mafia':'mafia.n.01', 'Denpa':'', 'Religion':'religion.n.01', 'Slapstick':'', 'Historical':'historical.a.01',
    'Fugitive':'fugitive.n.01', 'Astronomy':'astronomy.n.01', 'Battle Royale':'battle.n.01', 'Super Power':'superpower.n.01', 'Guns':'gun.n.01', 'Scuba Diving':'scuba_diving.n.01 sport.n.01',
    'Work':'employment.n.02', 'Youkai':'devil.n.02 monster.n.01', 'Puppetry':'puppetry.n.01', 'Biographical':'biographic.a.01', 'Kids':'child.n.01', 'Death Game':'death.n.01',
    'Cheerleading':'cheerleader.n.02', 'Zombie':'zombi.n.03 monster.n.01', 'Fishing':'fishing.n.01', 'Real Robot':'automaton.n.02', "Teens' Love":'love.n.01', 'Demons':'devil.n.02 monster.n.01',
    'Judo':'judo.n.01', 'Teacher':'teacher.n.01', 'Golf':'golf.n.01', 'Bullying':'bullying.n.01', 'Wrestling':'wrestling.n.02', 'War':'war.n.01', 'Skateboarding':'skateboarding.n.01',
    'Tanned Skin':'tan.v.01', 'Centaur':'centaur.n.01', 'American Football':'american_football.n.01 sport.n.01', 'Acting':'acting.n.01 ', 'Mahjong':'mah-jongg.n.01', 'Terrorism':'terrorism.n.01',
    'Classic Literature':'literature.n.01', 'Agriculture':'farming.n.01', 'Space': 'outer_space.n.01', 'Fashion':'fashion.n.04', 'Butler':'butler.n.01',
    'Memory Manipulation':'manipulation.n.01', 'Dungeon':'dungeon.n.02', 'Rugby':'rugby.n.01 sport.n.01', 'Werewolf':'werewolf.n.01', 'Crime':'crime.n.01', 'Cycling':'cycling.n.01 bicycle.n.01',
    'Politics':'politics.n.01', 'Police':'police.n.01', 'Pirates':'pirate.n.02', 'Villainess':'villainess.n.01', 'Martial Arts':'martial_art.n.01 sport.n.01', 'Body Horror':'horror.n.01',
    'Cosmic Horror':'horror.n.01', 'Abuse':'maltreatment.n.01'
}

#schema documenti nel indice
schema = Schema(type=KEYWORD(stored=True, lowercase=True), \
    title=TEXT(stored=True, field_boost=1.5, analyzer=StemmingAnalyzer()), \
        en_title=TEXT(stored=True, field_boost=1.5, analyzer=StemmingAnalyzer()), \
            synonyms=TEXT(stored=True, field_boost=1.2, analyzer=StemmingAnalyzer()), \
                format=KEYWORD(stored=True, lowercase=True, analyzer=StemmingAnalyzer()), \
                    status=KEYWORD(stored=True, lowercase=True, analyzer=StemmingAnalyzer()), \
                        description=TEXT(stored=True, analyzer=StemmingAnalyzer()), \
                            year=NUMERIC(int, stored=True), \
                                episodes=NUMERIC(int, stored=True), \
                                    duration=NUMERIC(int, stored=True), \
                                        chapters=NUMERIC(int, stored=True), \
                                            volumes=NUMERIC(int, stored=True), \
                                                genres=KEYWORD(stored=True, lowercase=True, commas=True, analyzer=StemmingAnalyzer(), scorable=True), \
                                                    tags=KEYWORD(stored=True, lowercase=True, commas=True, analyzer=StemmingAnalyzer(), scorable=True), \
                                                        content=KEYWORD(stored=True, scorable=True), \
                                                            score=NUMERIC(int, stored=True, sortable=True), \
                                                                url=ID(stored=True), \
                                                                    image=ID(stored=True), \
                                                                        docName=ID(stored=True))



#creazione indice
if not os.path.exists("indexdir"):
    os.mkdir("indexdir")

ix = None

res = None
while (res != '1') and (res != '2'):
    print("Vuoi creare un nuovo indice(1) oppure usarne uno già esistente(2):")
    res = (input())

if res == '1':
    print("Nome nuovo indice: ", end='')
    name = input()
    while index.exists_in("indexdir", indexname=name):
        print("Indice già presente.\nInserire nuovo nome:", end='')
        name = input()
    ix = index.create_in("indexdir", schema=schema, indexname=name)

elif res == '2':
    print("Nome indice: ")
    name = input()
    while not index.exists_in("indexdir", indexname=name):
        print("Indice non presente.\nInserire nuovo nome:", end='')
        name = input()
    ix = index.open_dir("indexdir", indexname=name)

mode = None
while (mode != '1') and (mode != '2'):
    print("Modalità (1:manuale, 2:automatico):", end=" ")
    mode = input()

hyper = lambda s: s.hypernyms()

def selectKeyword(text):
    
    keywords = []
    print(text)

    r = Rake(max_length=1)
    r.extract_keywords_from_text(text)
    words = dict(enumerate(word for word in r.get_ranked_phrases()))

    for word in words.items():
        print(word)
    print("\nSeleziona le keyword (q - per finire):")
    while len(keywords) < 3:
        key = input()
        if(key == 'q'):
            break
        if key.isdecimal() and int(key) < len(words) and int(key) >= 0 :
            key = int(key)
            keywords.append(words.get(key))
        else:
            print("Valore inserito non valido")

    if len(keywords) < 3:
        print("\nInserisci le keyword manualmente:")
        while len(keywords)<3:
            word = input()
            keywords.append(word)
    
    return keywords

def selectSyns(text, words):

    synsets = []

    for word in words:
        syn = lesk(text.split(), word)
        print(syn)
        if(syn):
            print(syn, syn.definition())
            print('Corretto(s/n)?')
            if input() == 'n':
                syn = wn.synsets(word)
                id = 0
                for s in syn:
                    print(id, s, s.definition())
                    id += 1
                while (id < 0) or (id >= len(syn)):
                    try:
                        id = int(input("Quale?:"))
                    except:
                        print("Non valido")
                synsets.append(syn[id].name())
                for l in syn[id].lemmas():
                    synsets.append((wn.synsets(l.name()))[0].name())
            else:
                synsets.append(syn.name())
                for l in syn.lemmas():
                    synsets.append((wn.synsets(l.name()))[0].name())
            try:
                for h in syn.closure(hyper, depth=5):
                    synsets.append(h.name())
            except:
                continue
    return synsets

def tagsContent(tags, synsets):

    for tag in tags:
        if tag in tagsDict:
            for t in tagsDict[tag].split():
                synsets.append(t)
                try:
                    syn = wn.synset(t)
                    for h in syn.closure(hyper, depth=2):
                        synsets.append(h.name())
                except:
                    continue

    synsets = list(dict.fromkeys(synsets))

    content = ""
    for word in synsets:
        content += str(word) + " "
    return content

def getContentMan(text, tags):

    keyword = selectKeyword(text)
    synsets = selectSyns(text, keyword)
    
    content = tagsContent(tags, synsets)
    
    return content

def getContentAuto(tags):

    synsets = []

    content = tagsContent(tags, synsets)

    return content

writer = ix.writer()
reader = ix.reader()

counter = 0

for filename in os.listdir(docDir):

    if (reader.doc_frequency("docName", filename)) == 0:

        f = open(docDir+'/'+filename)
        data = json.load(f)
        data = ((data["data"])["Media"])

        if data["isAdult"] == False:
            counter +=1
            print(counter)            
            type = data["type"]
            title = data["title"]["romaji"]
            en_title = data["title"]["english"]
            synonyms = ''
            for synonym in data["synonyms"]:
                synonyms += synonym+", "
            synonyms = synonyms.rstrip(', ')
            format = data['format']
            status = data["status"]
            if data["description"] != None:
                description = data["description"]
            else:
                description = ""
            year = data["startDate"]["year"]
            episodes = data["episodes"]
            duration = data["duration"]
            chapters = data["chapters"]
            volumes = data["volumes"]
            genres=''
            for genre in list(dict.fromkeys(data["genres"])):
                genres += genre+", "
            genres = genres.rstrip(', ')
            tags = []
            for tag in data["tags"]:
                if tag["isAdult"]==False:
                    tags.append(tag["name"])
            #Modalità manuale
            if(mode == "1"):
                content = getContentMan(description, tags)
                print("Premi q per finire:")
                resp = input()
                if resp == 'q':
                    break
            #Modalità automatica
            if(mode == "2"):
                content = getContentAuto(tags)
            try:
                score = int(data["averageScore"])
            except:
                score = 0
            url = data["siteUrl"]
            image = data["coverImage"]["extraLarge"]        
            
            tags = ','.join(tags)
            
            writer.add_document(type=type, title=title, en_title=en_title, synonyms=synonyms, format=format, status=status, description=description, year=year, episodes=episodes, \
                duration=duration, chapters=chapters, volumes=volumes, genres=genres, tags=tags, content=content, score=score, url=url,  image=image, docName=filename)
        f.close()
        
    else:
        print("Già indicizzato")

writer.commit()
reader.close()
ix.close()