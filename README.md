# owo finder

Project for the university course of Information Management

# Installazione

- `$ pip install -r requirements.txt`
- Scaricare il pacchetto wordnet in nltk.
    `$ python`
    ```
    import nltk
    ntlk.download()
    ```
- `$ python manage.py runserver`
- Il programma sarà in esecuzione al indirizzo: http://127.0.0.1:8000/

# Utilizzo
L'applicazione accetta query di tipo:
-multiple word query, le parole verranno cercate nel titolo (originale, inglese e sinonimi) e la descrizione, è possibile specificare il campo in cui cercare ( field:word )
-phrase query, la frase verrà carcata nel titolo (originale, inglese e sinonimi) e la descrizione
-boolean query, è possibile specificare operatori booleani (AND OR NOT) tra le parole, di default tutti gli elementi sono in OR
-ranges, utili per i campi come episodi, durata, capitoli, volumi, anno di uscita e voto
-concept-based query, è possibile specificare concetti che saranno ricercati nei documenti ( concept# )

Campi:

- type: tipologia di opera
- title: titolo originale
- en_title: titolo inglese
- synonyms: sinonimi
- description: descrizione
- format: formato di distribuzione
- status: status dell’opera
- year: anno di uscita
- episodes: numero di episodi (solo anime)
- duration: durata episodi (solo anime)
- chapters: numero di capitoli (solo manga)
- volumes: numero di volumi (solo manga)
- genres: genere dell’opera
- tags: tags dell’opera
- score: valutazione



tipi: ANIME, MANGA

formati: TV, SPECIAL, MANGA, TV_SHORT, ONE_SHOT, OVA, ONA, MOVIE, NOVEL, MUSIC

status: FINISHED, RELEASING, NOT_YET_RELEASED, CANCELLED

generi: Action, Adventure, Drama, Sci-Fi, Comedy, Fantasy, Mahou Shoujo, Romance, Supernatural, Slice of Life, Psychological, Mecha, Horror, Mystery, Sports, Thriller, Music

tags: Space, Crime, Episodic, Ensemble Cast, Primarily Adult Cast, Noir, Tragedy, Guns, Cyberpunk, Male Protagonist, Philosophy, Martial Arts, Anti-Hero, 
Amnesia, Gambling, Cyborg, Drugs, Yakuza, Tanned Skin, Nudity, Magic, Henshin, Male Harem, Maids, Fairy Tale, Shoujo, Female Protagonist, Family Life, 
Love Triangle, Swordplay, Aliens, Space Opera, Seinen, Pirates, Fugitive, Super Power, Primarily Female Cast, Shounen, School, Josei, Full Color, Band, 
Coming of Age, Musical, Office Lady, Time Skip, Heterosexual, Parody, Real Robot, Super Robot, Food, Robots, Surreal Comedy, War, Bullying, Witch, Gore, 
Urban Fantasy, Yuri, Idol, Time Manipulation, Body Horror, Photography, "Boys Love", Crossdressing, Rural, Primarily Male Cast, Work, Agriculture, Animals, 
Female Harem, Police, Swimming, Post-Apocalyptic, Cosmic Horror, Monster Girl, 4-koma, Gender Bending, School Club, Astronomy, Reincarnation, Demons, Age Gap, 
Isekai, Elf, Twins, Kuudere, LGBTQ+ Themes, Bisexual, Mythology, Revenge, Dragons, Ghost, Cute Girls Doing Cute Things, Iyashikei, Fitness, POV, Adoption, 
Satire, Slapstick, Urban, Bar, Anthology, Butler, Hikikomori, Video Games, Virtual World, CGI, Historical, Otaku Culture, Skeleton, Autobiographical, Educational, 
Language Barrier, Memory Manipulation, Slavery, Politics, Body Swapping, Vampire, Drawing, Primarily Child Cast, Dystopian, Shapeshifting, Goblin, Crossover, Meta, 
Succubus, Age Regression, Foreign, College, Religion, Chimera, Lost Civilization, Military, Dungeon, Conspiracy, Battle Royale, Alternate Universe, Samurai, Outdoor, 
Kemonomimi, Survival, Chuunibyou, Card Battle, Tsundere, Gods, Youkai, Kids, Werewolf, Teacher, Death Game, Superhero, Cultivation, Artificial Intelligence, Full CGI, 
Tennis, Chibi, Kaiju, Yandere, Tomboy, Nun, Cute Boys Doing Cute Things, Writing, Gangs, Dancing, Archery, Advertisement, Ojou-sama, Acting, Cars, Dissociative Identities, 
Detective, Terrorism, Dullahan, Mahjong, Ninja, Anachronism, Assassins, Zombie, Afterlife, Delinquents, Gyaru, Ships, Parkour, Boxing, Athletics, Baseball, Scuba Diving, 
Environmental, Motorcycles, Stop Motion, Steampunk, Augmented Reality, Rehabilitation, Mafia, Asexual, Achromatic, Table Tennis, No Dialogue, Denpa, Centaur, Nekomimi, 
Puppetry, Wuxia, Espionage, Suicide, Villainess, Dinosaurs, Shrine Maiden, Trains, Angels, Classic Literature, Cheerleading, Ice Skating, Mermaid, Reformation, 
Tokusatsu, Aviation, Software Development, Economics, Triads, Football, Cosplay, Firefighters, Volleyball, Teens Love, Medicine, Sumo, Achronological Order, 
Fashion, Lacrosse, Badminton, Flash, Wrestling, Karuta, Cannibalism, Basketball, Circus, Rakugo, Agender, Monster Boy, Rotoscoping, Vikings, Cult, E-Sports, 
Pandemic, Mopeds, Ero Guro, Go, Shogi, American Football, Biographical, Rugby, Fishing, Airsoft, Torture, Skateboarding, Tanks, Oiran, Poker, Cycling, Judo, 
VTuber, Surfing, Calligraphy, Golf, Abuse

Esempi di query:

death fight -> manga e anime che contengono le parole death o fight nel titolo o nella descrizione 

"dragon ball" -> manga e anime che contengono la frase "dragon ball" nel titolo o nella descrizione

title:conan -> manga e anime che contengono la parola conan nel titolo

type:anime AND year:1999 -> anime usciti nel 1999

format:movie AND year:>=2010 -> film usciti dal 2010

type:anime AND genres:thriller AND episodes:<20 AND duration:<30 -> anime di genere thriller con meno di 20 episodi con una durata inferiore a 30 min

type:manga AND score:>85 -> manga con uno score maggiore di 85

pirate# -> anime e manga a tema pirati (concept-based)

genres:(action sports) -> anime e manga di genere action o sport

type:manga AND genres:mecha AND NOT volumes:>10 -> manga di genere mecha con non più di 10 volumi

type:anime AND episodes:<15 type:manga AND chapters:<50 -> anime con meno di 15 episodi e manga con meno di 50 capitoli

status:finished -> anime e manga finiti